<?php

namespace App\Models;

class CitasMedico {
 
    public $numero;
    public $hora;
    public $historia;
    public $pacientes;
    public $confirmacion;
    public $numerocuenta;
    public $numeroorden;
    public $consultorio;
    public $linkzoom;
    public $atendido;
    public $cie10;
    public $celular;

}

